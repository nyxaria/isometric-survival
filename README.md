# isometric-survival
During the Summer of 2015, I set out to create a game using only vanilla Java. It is heavily inspired by Project Zomboid and was a great challange to me see how far I could take vanilla Java in creating a game that runs smoothly and efficently.
<br>

<br>
<p align="center"><img src="/isometry/assets/isogif.gif"></p>
